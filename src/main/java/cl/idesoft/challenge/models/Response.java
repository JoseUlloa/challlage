package cl.idesoft.challenge.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Response {
    private String status;
    @JsonProperty("data")
    private List<Holiday> holidays;
}
