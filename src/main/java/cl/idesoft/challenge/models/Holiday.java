package cl.idesoft.challenge.models;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class Holiday{
    private LocalDate date;
    private String title;
    private String type;
    private Boolean inalienable;
    private String extra;
}
