package cl.idesoft.challenge.controllers;

import cl.idesoft.challenge.config.handler.ErrorResponse;
import cl.idesoft.challenge.controllers.http.HolidayRequest;
import cl.idesoft.challenge.controllers.http.HolidayResponse;
import cl.idesoft.challenge.exception.NotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface HolidayApi {

    @Operation(summary = "Get list of holidays by the specified filter.")
    @ApiResponses(value = {

            @ApiResponse(responseCode = "200", description = "Found the holidays",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = HolidayResponse.class)) }),
            @ApiResponse(responseCode = "404", description = "Holidays not found",
                    content  = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponse.class)) }),
            @ApiResponse(responseCode = "500", description = "Something in the server was wrong",
                    content = @Content) })
    ResponseEntity<HolidayResponse> filter(@RequestBody HolidayRequest holidayRequest) throws Throwable;
}
