package cl.idesoft.challenge.controllers.http;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Builder
public class HolidayRequest {

    private String type;
    private LocalDate from;
    private LocalDate to;
}
