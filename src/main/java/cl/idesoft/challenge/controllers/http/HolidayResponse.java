package cl.idesoft.challenge.controllers.http;

import cl.idesoft.challenge.models.Holiday;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class HolidayResponse {

    private List<Holiday> holidays;
}
