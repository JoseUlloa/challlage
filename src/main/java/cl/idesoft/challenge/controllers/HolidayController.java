package cl.idesoft.challenge.controllers;

import cl.idesoft.challenge.controllers.http.HolidayRequest;
import cl.idesoft.challenge.controllers.http.HolidayResponse;
import cl.idesoft.challenge.exception.NotFoundException;
import cl.idesoft.challenge.services.HolidayService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController()
@RequestMapping("/api/hiliday")
@RequiredArgsConstructor
public class HolidayController implements HolidayApi{

	private final HolidayService holidayService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<HolidayResponse> filter(@RequestBody HolidayRequest holidayRequest) throws Throwable {
		return ResponseEntity.ok(holidayService.filter(holidayRequest));
	}

}
