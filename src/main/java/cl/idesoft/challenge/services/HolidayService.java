package cl.idesoft.challenge.services;

import cl.idesoft.challenge.controllers.http.HolidayRequest;
import cl.idesoft.challenge.controllers.http.HolidayResponse;
import cl.idesoft.challenge.exception.ChallengeException;
import cl.idesoft.challenge.exception.NotFoundException;
import cl.idesoft.challenge.models.Holiday;
import cl.idesoft.challenge.models.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
@RequiredArgsConstructor
public class HolidayService {

	@Value("${holiday.path}")
	private String path;
	private List<Holiday> holidays;
	private final RestTemplate restTemplate;

	public HolidayResponse filter(HolidayRequest holidayRequest) throws ChallengeException {
		Stream<Holiday> holidayStream = applyFilter(holidayRequest);

		List<Holiday> data = holidayStream.collect(Collectors.toList());
		if (data.isEmpty()){
			throw new NotFoundException("not found");
		}

		return HolidayResponse.builder()
				.holidays(data)
				.build();
	}

	private Stream<Holiday> applyFilter(HolidayRequest holidayRequest) throws ChallengeException {
		try {
			Stream<Holiday> holidayStream = holidays.stream();

			if (Objects.nonNull(holidayRequest)) {
				if (Objects.nonNull(holidayRequest.getType())){
					holidayStream = holidayStream.filter(h -> h.getType().equalsIgnoreCase(holidayRequest.getType()));
				}

				if (Objects.nonNull(holidayRequest.getFrom()) && Objects.nonNull(holidayRequest.getTo())){
					holidayStream = holidayStream.filter(h ->
							h.getDate().isAfter(holidayRequest.getFrom())
									&& h.getDate().isBefore(holidayRequest.getTo()));
				}
			}
			return  holidayStream;
		} catch (Exception exception) {
			throw new ChallengeException("Something in the server was wrong");
		}
	}

	public void loadAllHoliday(){
		log.info("Loading holidays");
		if (Objects.isNull(holidays) || holidays.isEmpty()){
			Response responseHoliday = restTemplate
					.getForObject(path, Response.class);
			if (Objects.nonNull(responseHoliday)){
				holidays = responseHoliday.getHolidays();
			}
		}
		log.info("upload finished");
	}
}
