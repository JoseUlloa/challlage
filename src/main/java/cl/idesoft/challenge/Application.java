package cl.idesoft.challenge;

import cl.idesoft.challenge.services.HolidayService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

		@Bean
	CommandLineRunner lookup(HolidayService holidayService) {
		holidayService.loadAllHoliday();
		return null;
	}
}
