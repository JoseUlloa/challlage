package cl.idesoft.challenge.config.handler;

import cl.idesoft.challenge.exception.ChallengeException;
import cl.idesoft.challenge.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
@RequiredArgsConstructor
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler implements GenericResponses {

    @ExceptionHandler(value = ChallengeException.class)
    public ResponseEntity<ErrorResponse> requestNotProcessed(ChallengeException ex) {
        return internalServerError(ex.getMessage());
    }

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<ErrorResponse> requestNotFound(NotFoundException ex) {
        return notFound(ex.getMessage());
    }
}
