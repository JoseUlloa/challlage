package cl.idesoft.challenge.config.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public interface GenericResponses {

    default ResponseEntity<ErrorResponse> internalServerError(String msg) {
        return new ResponseEntity<>(messageResponse(msg), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    default ResponseEntity<ErrorResponse> notFound(String msg) {
        return new ResponseEntity<>(messageResponse(msg), HttpStatus.NOT_FOUND);
    }

    default ErrorResponse messageResponse(String msg) {
        return new ErrorResponse(msg);
    }
}
