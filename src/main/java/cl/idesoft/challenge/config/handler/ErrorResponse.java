package cl.idesoft.challenge.config.handler;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class ErrorResponse {

	@Schema(name = "message", description = "${error.response.message.description}")
	@JsonProperty("message")
	private final String message;

}
