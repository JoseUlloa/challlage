package cl.idesoft.challenge.exception;


public class ChallengeException extends Exception {

    public static final long serialVersionUID = 0L;

    public ChallengeException(String msg) {
        super(msg);
    }
}