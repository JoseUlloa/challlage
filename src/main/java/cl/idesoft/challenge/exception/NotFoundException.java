package cl.idesoft.challenge.exception;

public class NotFoundException extends ChallengeException {

    public static final long serialVersionUID = 0L;

    public NotFoundException(String msg) {
        super(msg);
    }
}