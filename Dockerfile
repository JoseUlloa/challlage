FROM openjdk:11-jdk-alpine
LABEL maintainer="josemanuel.ulloavasquez@gmail.com"
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} lukasmd-app.jar
ENTRYPOINT ["java", "-Djava.awt.headless=true", "-jar", "challage-app.jar"]